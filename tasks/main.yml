---

- name: ensure config dirs
  file:
    path: /etc/{{item}}
    state: directory
  loop:
    - dcomp
    - cogs
    - foundry
    - merge

- name: copy commander keys
  copy:
    src: "{{cmdr_cert}}"
    dest: /etc/merge/cmdr.pem

- name: copy testbed model
  copy:
    src: "{{item.src}}" 
    dest: "{{item.dest}}"
  loop:
    - src: "{{tbxir}}"
      dest: /etc/dcomp/tb-xir.json

    - src: "{{tbxir}}"
      dest: /etc/cogs/tb-xir.json

- name: copy config files
  template:
    src: cogs.yml
    dest: /etc/dcomp/cogs.yml

- name: copy rex and cog binaries
  get_url:
    url: https://gitlab.com/dcomptb/cogs/-/jobs/artifacts/v0.2.11/raw/build/{{item}}?job=build
    dest: /usr/local/bin/{{item}}
    force: yes
    mode: a+x
  register: result
  until: result.failed == False
  retries: 3
  delay: 3

  loop:
    - rex
    - cog
    - driver

- name: rex etcd proxy
  import_role:
    name: etcd
  vars:
    proxy: yes
    proxy_name: rex
    proxy_port: 2399
    proxy_verify_peer: no
    proxy_endpoints: "{{etcd_cluster}}"
    ca_pem: "{{etcd_ca}}"
    db_pem: "{{etcd_cert}}"
    db_key: "{{etcd_key}}"

- name: copy services
  template:
    src: "{{item}}.service"
    dest: /lib/systemd/system/{{item}}.service
  loop:
    - rex
    - driver
    - beluga

- name: enable services
  systemd:
    enabled: true
    name: "{{item}}"
    daemon_reload: true
  loop:
    - rex
    - driver

- name: start services
  service:
    name: "{{item}}"
    state: restarted
  loop:
    - rex
    - driver

- name: install canopy
  include_role:
    name: canopy
  vars:
    client: yes
    server: yes
    arch: amd64
    platform: debian

- name: setup GoBGP/Gobble
  include_role:
    name: gobble
  vars:
    interface: "{{tbifx}}"
    with_gobgp: yes
    router_id: "{{bgp_id}}"
    router_as: "{{bgp_as}}"
    peer_as: "{{bgp_peer_as}}"

- name: fetch nex client
  get_url:
    url: https://gitlab.com/mergetb/tech/nex/-/jobs/artifacts/v0.5.4/raw/build/nexc?job=build
    dest: /usr/local/bin/nex
    force: yes
    mode: a+x
  register: result
  until: result.failed == False
  retries: 3
  delay: 3

- name: fetch nl utility (iproute2 like thing)
  get_url:
    url: https://gitlab.com/mergetb/tech/rtnl/-/jobs/artifacts/v0.1.6/raw/build/nl?job=test
    dest: /usr/local/bin/nl
    force: yes
    mode: a+x
  register: result
  until: result.failed == False
  retries: 3
  delay: 3

# this is for provisioning nex containers, not a local nex
- name: copy container nex config
  template:
    src: nex.yml
    dest: /etc/cogs/nex.yml

- name: install deps
  apt:
    name: 
    - cgroup-tools
    - libseccomp2
    - bridge-utils
    - iptables
    - iptables-persistent
    state: latest

- name: configure ipv4 forwarding
  lineinfile:
    path: /etc/sysctl.conf
    line: "{{item}}" 
  loop:
    - net.ipv4.ip_forward=1
    - net.ipv4.neigh.default.gc_thresh2=2048
    - net.ipv4.neigh.default.gc_thresh3=4096

- name: enable sysctl settings
  shell: sysctl -p

- name: bring down loopback
  shell: /sbin/ifdown lo

- name: bgp loopback address
  blockinfile:
    path: /etc/network/interfaces
    block: |
      auto lo
      iface lo inet static
        address {{bgp_addr}}

      auto {{tbifx}}
      iface {{tbifx}} inet manual
        post-up ip link set mtu 9216 dev {{tbifx}}

- name: bring up loopback
  shell: /sbin/ifup lo

- name: bring up tbifx
  shell: /sbin/ifup {{tbifx}}

- name: blacklist nftables
  copy:
    dest: /etc/modprobe.d/nftables.conf
    content: |
      blacklist nft*

- name: ensure iptables persistent dir
  file:
    path: /etc/iptables
    state: directory

- name: copy iptables persistent rules
  template:
    src: enclave.v4
    dest: /etc/iptables/rules.v4

- name: activate enclave iptables rules
  shell: iptables-legacy-restore < /etc/iptables/rules.v4

- name: download tarball
  get_url:
    url: https://github.com/containerd/containerd/releases/download/v1.3.0/containerd-1.3.0.linux-amd64.tar.gz
    dest: /tmp/containerd-1.3.0.linux-amd64.tar.gz
  register: result
  until: result.failed == False
  retries: 3
  delay: 3

- name: unpack tarball / install
  unarchive:
    src: /tmp/containerd-1.3.0.linux-amd64.tar.gz
    dest: /usr/local/
    remote_src: true

- name: fetch runc
  get_url:
    url: https://github.com/opencontainers/runc/releases/download/v1.0.0-rc9/runc.amd64
    dest: /usr/bin/runc
    mode: a+x
  register: result
  until: result.failed == False
  retries: 3
  delay: 3

- name: create systemd entry
  copy:
    src: containerd.service
    dest: /lib/systemd/system/containerd.service


- name: enable containerd service
  systemd:
    name: containerd
    daemon_reload: true
    enabled: true

- name: start containerd service
  service:
    name: containerd
    state: restarted

- file:
    path: /etc/bash_completion.d
    state: directory

- copy:
    src: cog-auto
    dest: /etc/bash_completion.d/cog

- name: install foundry tls key material
  copy:
    src: "{{item.src}}"
    dest: "{{item.dest}}"
  loop:
    - src: "{{foundry_key}}"
      dest: /etc/foundry/manage-key.pem
    - src: "{{foundry_cert}}"
      dest: /etc/foundry/manage.pem

- name: install foundry management CLI
  get_url:
    url: https://gitlab.com/mergetb/tech/foundry/-/jobs/artifacts/v0.1.12/raw/build/foundry?job=build
    dest: /usr/local/bin/foundry
    mode: 0755
  register: result
  until: result.failed == False
  retries: 3
  delay: 3

- name: get beluga binaries
  get_url:
    url: https://gitlab.com/mergetb/tech/beluga/-/jobs/artifacts/v0.1.7/raw/build/{{item}}?job=build
    dest: /usr/local/bin/{{item}}
    mode: 0755
  register: result
  until: result.failed == False
  retries: 3
  delay: 3
  loop:
    - belugad
    - beluga

- name: install harbor task specs
  template:
    src: "{{item}}.yml"
    dest: /etc/cogs/{{item}}.yml
  loop:
    - harbor-services-up
    - harbor-services-down
    - harbor-vtep-up
